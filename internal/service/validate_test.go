package validate

import "testing"

func Test_isValid(t *testing.T){
    tt := []struct{
        name string
        target int
        want bool
    }{
        {
            name:"should return true when target is 1",
            target: 1,
            want: true,
        },
        {
            name:"should return false when target is less than 1",
            target: 0,
            want: false,
        },
    }

    for _, tc := range tt{
        t.Run(tc.name, func(t *testing.T){
            got := IsValid(tc.target)
            if tc.want != got{
                t.Errorf("want %v but got %v", tc.want, got)
            }
        })
    }
}

